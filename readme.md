About this repository
=====================

The Wolf CMS project uses Github for their git repository. Unfortunately, Github decided no longer to support providing custom downloads.

For this reason, we've created this repository on BitBucket. It will host all of our downloads. While doing so, we will also be evaluating moving from Github to Bitbucket permanently.

Sincerely, the Wolf CMS team.
